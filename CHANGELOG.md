# Moon.OData changelog

3.0.1

- All libraries are targeting .NET Standard.

2.2.6

- Removed unnecessary dependencies.